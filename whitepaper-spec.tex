\documentclass[a4paper,11pt]{article}

\usepackage{multicol}
\usepackage{enumitem}

\usepackage{amsthm}
\usepackage{tikz}
\usepackage{graphicx}
\usepackage[hidelinks]{hyperref}

\usetikzlibrary{positioning, chains, calc}

% https://stackoverflow.com/questions/1465665/passing-command-line-arguments-to-latex-document/1466610#1466610
\providecommand{\nda}[2]{\texttt{#1}}
\newtheorem{theorem}{Theorem}
\newtheorem{definition}{Definition}[theorem]

\tikzset{
	flownode/.style={
		rectangle,
		rounded corners,
		% fill=black!10,
		draw=black, very thick,
		text width=5.5em,
		minimum height=3em,
		text centered,
		on chain,
	},
	every join/.style={->, thick},
}

\begin{document}

\title{The PlanckPay Specification \\ \vspace{.2cm}
	\large Federation of micropayment systems}
\author{Oscar Leijendekker}

\maketitle

\begin{abstract}
The adoption of micropayments is hindered by both hard technical limitations and
the difficult co-dependence between market and processors. We introduce a
federation protocol that seeks to circumvent both issues by quantifying trust in
adhering initiatives through automated audits.
\end{abstract}

\begin{multicols}{2}

\section{Introduction}
It is well understood that pay-by-use micropayments have great potential for
the monetization of web content and services \cite{brin2016advertising, lochy2019micro, mercator2004thewhole, mercator2005anticipating, herzberg1998content, zager2005spam, nelson1997transcopyright}.
The rise of IoT, and the potential of further decreased payment values in
machine-to-machine transactions further suggests an upcoming need for
micropayments. Many systems have been designed to handle micro payments
\cite{micali2002micropayments, rivest1996payword, rivest1997electronic, rivest2004peppercoin, allison2016blockchain},
and we've encountered multiple companies, both past and current, that offer
micropayment services.
Why then, have none of these systems become widely adopted?

While some would argue micropayments have been superseded by advertisements \cite{mercator2007solving},
we find this argument unconvincing. Economically, the time/value proposition of
advertisements is often below minimum wage and the expected cost to the consumer
is higher than for micropayments, as the overheads and profits of the advertisers
must be met. While the latter point may be inaccurate \cite{fou2021when-big-brands},
that too does not bode well for the advertisement industry. From a practical
standpoint, we observe the success of Patreon and similar platforms, and note
that many digital newspapers have adopted paywalls. These observations suggest
that there is a market for paid digital content.

Instead, micropayments are much harder to develop than advertisements, both from
 a market and technical standpoint. In the market, sellers require a broadly-supported
micropayment system with many users, whereas companies with the clout to
create such systems will not do so without seeing an existing market.
Technically payments are linear operations, and do not scale horizontally as
advertisements do. Proposals with the foresight to recognize this issue suffer
from a variety of issues as they struggle to meet ultimately incompatible
requirements.

We propose a federation protocol that aims to unify the interfaces of
micropayment providers, thereby creating a much stronger platform for new
markets to develop, and uses measured trust to bring some flexibility into the
technical requirements.

This document only globally covers the specification, which is instead presented
in detail in \cite{planckpay-spec}.


\section{Solving the market problem}
The current market for micropayment systems consists of a multitude of small,
incompatible payment systems. A seller wishing to use micropayments must either
rely on only one PSP (Payment System Provider), which limits its target audience
to users of that PSP, or support many PSPs, which not only requires serious
effort but also negatively affects UX.

Each PSP maintains its own frontend plugin. Some of these plugins can be
somewhat intrusive. E.g. they may occupy a large amount of the screen real
estate or present a pop-up. There is simply no way to cleanly combine such
systems without hiding them in a sub-menu and lengthening the checkout process
for a payment sum that is likely not worth the extra time.
Each provider also has its own system to verify payment success on the backend.
Clearly integration of multiple micropayment systems is not practical, and we
note that, even for regular payments, support for multiple payment methods is
usually offered through specialized intermediaries like Adyen and Mollie.

The PlanckPay Specification creates an open standard for micropayment handling.
PSPs adhering to the standard can all operate on the same frontend interface,
and payment validation happens through the same method regardless of PSP.
This means vendors can sell to users of many PSPs, and adhering PSPs can be used
for a variety of goods and services, not just the once they've contracted
themselves. Thus a broadly supported basis for micropayment markets can be
developed.

\section{Solving the technical issues}
Scaling down payments introduces difficult technical requirements that are often
overlooked. First, the burden we can put upon the user for payments of
sufficiently low value is near zero. This includes mental accounting costs,
actions, and even time \cite{szabo1999micropayments, odlyzko2003against-micropayments}.
We must therefore expect that part of the payment process is left to machines\footnote{This was, of course, always a given for the IoT sector},
in particular for authentication. This in turn legally requires strict amortized
spending limits \cite{EU-2018/389}. Even without legal requirements, we
expect the prospect of software error or malicious entities causing damages
without upper bound, a real possibility when relaxing authentication requirements
without introducing hard spending limits, to be a showstopper.

Maintaining strict spending limits requires either solving the double spending
problem before completing the transaction, which makes payments linear
operations, or assigning losses from double spending to either the PSP or the
vendor. Unfortunately, linear operations do not scale well, and attempting to
keep a highly available, high-throughput payment system will result in errors \cite{brewer2000cap, gilbert2002cap}.

To circumvent these issues, the PlanckPay specification accepts that PSPs'
double spending prevention mechanisms are heuristics and necessarily imperfect.
If double spending occurs, the vendor is not credited. To protect the vendor's
interest, the PlanckPay specification allows continuous monitoring of promised
and delivered payments, so that PSPs' effectiveness in preventing double
spending attacks can be measured, and failing PSPs can be blocked or incur an
additional risk charge.


\section{Specification overview}
We give a general overview of how payments in the PlanckPay specification work.
A buyer sends a payment request to his PSP, e.g. by clicking on a secure ``pay''
button. The PSP accepts or rejects the payment based on its own heuristics, and
sends a digitally signed response to the client. If the payment is accepted, the
client forwards the response, which in this case we will call a receipt, to the vendor.
Eventually, if no double-spending has occurred, the vendor receives
aggregated payments from the PSP over a regular payment channel, with a
reference to the receipt. The vendor can then compare received payments with
received receipts, and because that data is signed and contained no client
information, share that data with other vendors or watchdogs.

A PSP may of course choose to carry the costs of double spending themselves, but
in the micropayment market, where marginal costs are close to zero, they run
greater risks than vendors against double spending attacks.

\end{multicols}

\bibliography{bibliography}
\bibliographystyle{ieeetr}

\end{document}
